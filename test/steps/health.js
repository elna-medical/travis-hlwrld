import { Given, Then } from 'cucumber';

Given('I check for the health of the server',  () => {
    browser.url('/health');
    expect(browser).toHaveUrl('http://localhost:3000/health');
    
});

Then('I see that the server is healthy',  () => {
    const text = browser.$('body').getText();
    expect(text).toBe('healthy system!')
});