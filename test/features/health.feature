Feature: Health check

    I want the server to be healthy

    Scenario: Health check
        Given I check for the health of the server
        Then I see that the server is healthy