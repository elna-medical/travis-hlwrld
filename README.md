
## Description

NestJS and Kubernetes PoC with readiness and liveness probes. The application is built to crash and restart automatically to test the probes functionality.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Docker & Kubernetes

### Building the image
```bash
$ docker build -t kslacroix/nestpoc .
```

### Testing the image
```bash
$ docker run -p 3000:3000 kslacroix/nestpoc
```
1. to http://localhost:3000 in your browser. You should see a placeholder list.
2. stop the container with ^C in your console

### Deploying on a Kubernetes cluster

Using minikube
```bash
# start the cluster
$ minikube start

# monitor the cluster
$ kubectl get pod -w
```

Don't close this terminal. Open a second terminal and type the following

```bash
# deploy the app
$ kubectl apply -f deployment.yml

# print the service url
$ minikube service nestjs-poc
```
* The last line should should open your browser at the right URL

* The app might not work and it is intended that way. Return to the first terminal window and watch the status of your pod. it could restart from time to time. In fact the app has 1/3 chances to restart every 5sec. 
* To change this behavior go to the file location.controller.ts and change the parameter crash from true to false.
