# docker build -t mpocregistry.azurecr.io/kslacroix/nestpoc .
# docker push mpocregistry.azurecr.io/kslacroix/nestpoc
# docker run -p 3000:3000 kslacroix/nestpoc
# OR
# az configure --defaults acr=mpocregistry
# az acr build -t kslacroix/nestpoc-test .
FROM node:12 AS builder
WORKDIR /app
COPY ./package.json ./
RUN npm install
COPY . .
RUN npm run build

FROM node:12-alpine
WORKDIR /app
COPY --from=builder /app ./
CMD ["npm", "run", "start:prod"]