import { Module } from '@nestjs/common';
import { LocationModule } from './location/location.module';
//Config Module: https://docs.nestjs.com/techniques/configuration#configuration
import { ConfigModule } from '@nestjs/config';

@Module({
    imports: [LocationModule, ConfigModule.forRoot()],
})
export class AppModule {}