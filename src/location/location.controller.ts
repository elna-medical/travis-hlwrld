import { Controller, Get, Res } from '@nestjs/common';
import { Response } from 'express';
import { LocationService } from './location.service';

interface ILocationListDto {
  locations: string[];
}

@Controller()
export class LocationController {

  crash : boolean; 

  constructor(private readonly locationService: LocationService) {
    this.crash = false; //set to true to simulate crashes
  }

  @Get()
  listLocations(): ILocationListDto {
    const locations = this.locationService.list();
    return { locations };
  }

  //Any code greater than or equal to 200 and less than 400 indicates success. Any other code indicates failure.
  //https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/#define-a-liveness-http-request
  @Get('/health')
  probServer(@Res() res: Response) {
    let stat = 200; 
    if (this.crash) {
      stat = Math.floor(Math.random() * 300) + 200;  // number between 300 and 500
    }
    res.status(stat).send('healthy system!');
  }
}
